package Character;

import org.junit.jupiter.api.Test;
import org.testng.Assert;

import static org.junit.jupiter.api.Assertions.*;

class NameGeneratorTest {
    NameGenerator nameGenerator = new NameGenerator(true, true);

    @Test
    void testRandomName() {
        Assert.assertNotEquals(nameGenerator.getFirstName(), "Fiona");
        Assert.assertNotEquals(nameGenerator.getMiddleName(), "Pierce");
        Assert.assertNotEquals(nameGenerator.getLastName(), "Willbottom");
    }

    @Test
    void setFirstName() {
        nameGenerator.setFirstName("Green");
        Assert.assertEquals(nameGenerator.getFirstName(), "Green");
    }

    @Test
    void setLastName() {
        nameGenerator.setLastName("Bag");
        Assert.assertEquals(nameGenerator.getLastName(), "Bag");
    }

    @Test
    void setMiddleName() {
        nameGenerator.setMiddleName("Plastic");
        Assert.assertEquals(nameGenerator.getMiddleName(), "Plastic");
    }
}
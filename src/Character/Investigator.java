package Character;

/**
 * A class representing an investigator
 * @author Jeremy Dostal-Sharp
 * @version (23/03/2020)
 */
public class Investigator {
    //General investigator variables
    private String investigatorName;
    private String playerName;
    private boolean hasMiddleName;
    private Occupation occupation;
    private boolean female = true;
    private String residence;
    private String birthPlace;

    //Investigator specific variables
    private Characteristics characteristics;
    private Skills skills;
    private WeaponsAndCombat weaponsAndCombat;
    private Backstory backstory;
    private GearAndPossessions gearAndPossessions;
    private CashAndAssets cashAndAssets;

    //Objects that can be reused
    private NameGenerator nameGenerator;

    public Investigator(String playerName) {

    }

    /**
     * Sets the investigator name
     */
    public void getName() {
        getNameGenerator().changeFemale(isFemale());
        getNameGenerator().setmName(hasMiddleName);
        getNameGenerator().generateNewName();
        setInvestigatorName(getNameGenerator().toString());
    }



    public NameGenerator getNameGenerator() {
        return this.nameGenerator;
    }
    public String getInvestigatorName() {
        return investigatorName;
    }

    public void setInvestigatorName(String investigatorName) {
        this.investigatorName = investigatorName;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public boolean isFemale() {
        return female;
    }

    public void setFemale(boolean female) {
        this.female = female;
    }

    public boolean hasMiddleName(){
        return hasMiddleName;
    }

    public void setHasMiddleName(boolean hasMiddleName){
        this.hasMiddleName = hasMiddleName;
    }

    public String getResidence() {
        return residence;
    }

    public void setResidence(String residence) {
        this.residence = residence;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }
}

package Character;

/**
 * A class representing a random generator for an investigators characteristics
 * @author Jeremy Dostal-Sharp
 * @version (23/03/2020)
 */
public class Characteristics {
    private int age = -1;

    private int strength = 0;
    private int constitution = 0;
    private int size = 0;
    private int dexterity = 0;
    private int appearance = 0;
    private int education = 0;
    private int intelligence = 0;
    private int power = 0;
    private int move = 0;

    private int hitPoints = 0;
    private int maxHP = 0;
    private int sanity = 0;
    private int maxSanity =0;
    private int luck = 0;
    private int magicPoints = 0;
    private int maxMp = 0;

    private DiceRoll diceRoll;

    public Characteristics() {
        this.diceRoll = new DiceRoll();
        rerollStats();
    }

    /**
     * Reroll investigator stats
     */
    private void rerollStats() {
        setStrength(statRoll(3,6) * 5);
        setConstitution(statRoll(3,6) * 5);
        setSize((statRoll(2,6) + 6) * 5);
        setDexterity(statRoll(3,6) * 5);
        setAppearance(statRoll(3,6) * 5);
        setIntelligence((statRoll(2,6) + 6) * 5);
        setPower(statRoll(3,6) * 5);
        setEducation((statRoll(2,6) + 6) * 5);
        setLuck(statRoll(3,6) * 5);
        setMaxHP((getConstitution() + getSize())/10);
        generateAge(90, 15);
        ageModifications();
        generateMove();
        setMaxMp(getPower()/5);
        setMagicPoints(getMaxHP());
        setSanity(getPower());
        setMaxHP((getConstitution() + getSize())/10);
        setHitPoints(getMaxHP());
        setMaxSanity(99);
    }

    /**
     * Generates character move from there str and dex scores and reduces it
     * the older they are
     */
    private void generateMove() {
        int move = 0;
        int dex = getDexterity();
        int str = getStrength();
        int siz = getSize();
        if (dex < siz && str < siz){
            move = 7;
        } else if(str < siz || dex < siz) {
            move = 8;
        } else if(dex > siz && str > siz) {
            move = 9;
        }

        //Modify move due to age
        int age = getAge();
        if (age > 39 && age < 50) {
            move--;
        } else if (age > 49 && age < 60) {
            move -= 2;
        } else if (age > 59 && age < 70) {
            move -= 3;
        } else if (age > 69 && age < 80) {
            move -= 4;
        } else if (age >79) {
            move -= 5;
        }

        setMove(move);
    }

    /**
     * Rolls for a stat depending on the inputs given
     * @param numDiceToRoll how many dice of sides to roll.
     * @param sides the amount of sides on a dice to roll.
     * @return an int >= 0;
     */
    public int statRoll(int numDiceToRoll, int sides) {
        int stat = 0;
        for (int i = 0; i < numDiceToRoll; i++) {
            stat += diceRoll.getDiceRoll(sides);
        }
        return stat;
    }

    /**
     * Generates the age of the Investigator or if the min and max age
     * are out of bounds then it sets age to 25.
     * @param maxAge < 100;
     * @param minAge > 14;
     */
    public void generateAge(int maxAge, int minAge){
        if(maxAge <= 90 && minAge > 14 && maxAge >= minAge) {
            int ageGenerated = diceRoll.getDiceRoll(maxAge - minAge) + minAge;
            setAge(ageGenerated);
        } else {
            setAge(30);
        }
    }

    /**
     * Modifies stats for investigators age
     */
    public void ageModifications(){
        int age = getAge();
        int statChange = 0;
        if (age < 20) {
            ageModStat(5,true);
            setEducation(getEducation() - 5);
            for(int i = 0; i < 2; i++) {
                statChange = (statRoll(3,6) * 5);
                if (getLuck() < statChange) {
                    setLuck(statChange);
                }
            }
        } else if (age < 40 && age > 19) {
            educationCheck(1);
        } else if (age < 50 && age > 39) {
            educationCheck(2);
            ageModStat(5,false);
            setAppearance(getAppearance() - 5);
        } else if (age < 60 && age > 49) {
            educationCheck(3);
            ageModStat(10,false);
            setAppearance(getAppearance() - 10);
        } else if (age < 70 && age > 59) {
            educationCheck(4);
            ageModStat(20,false);
            setAppearance(getAppearance() - 15);
        } else if (age < 80 && age > 69) {
            educationCheck(4);
            ageModStat(40,false);
            setAppearance(getAppearance() - 20);
        } else if (age <= 90 && age > 79) {
            educationCheck(4);
            ageModStat(80,false);
            setAppearance(getAppearance() - 25);
        }
    }

    /**
     * Checks to see if there is an education increase
     * @param times how many times to check.
     */
    private void educationCheck(int times){
        for(int i = 0; i < times; i++){
            if(getEducation() < 99){
                if(getEducation() < diceRoll.getDiceRoll(100)) {
                    int improve = diceRoll.getDiceRoll(10);
                    if((getEducation() + improve) > 99){
                        setEducation(99);
                    }
                }
            } else {
                break;
            }
        }
    }

    /**
     * private helper method for ageModification() modifies stats depending
     * on age.
     * @param points amount of points that are randomly distributed
     * @param underTwenty if age is under 20
     */
    private void ageModStat(int points, boolean underTwenty){
        int first = 0;
        int second = 0;
        int third = 0;
        if(underTwenty) {
            first = diceRoll.getDiceRoll(points);
            second = points - first;
            setStrength(getStrength()-first);
            setSize(getSize()-second);
        } else {
            //Randomly reduce Strength, Constitution and Dexterity.
            first = diceRoll.getDiceRoll(points);
            second = diceRoll.getDiceRoll(points - first);
            third = points - (first + second);
            setStrength(getStrength()-first);
            setConstitution(getConstitution()-second);
            setDexterity(getDexterity()-third);
        }
    }

    /**
     *  Getters and Setters
     */

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return this.age;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getConstitution() {
        return constitution;
    }

    public void setConstitution(int constitution) {
        this.constitution = constitution;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getAppearance() {
        return appearance;
    }

    public void setAppearance(int appearance) {
        this.appearance = appearance;
    }

    public int getEducation() {
        return education;
    }

    public void setEducation(int education) {
        this.education = education;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getMove() {
        return move;
    }

    public void setMove(int move) {
        this.move = move;
    }

    public int getHitPoints() {
        return hitPoints;
    }

    public void setHitPoints(int hitPoints) {
        this.hitPoints = hitPoints;
    }

    public int getMaxHP() {
        return maxHP;
    }

    public void setMaxHP(int maxHP) {
        this.maxHP = maxHP;
    }

    public int getSanity() {
        return sanity;
    }

    public void setSanity(int sanity) {
        this.sanity = sanity;
    }

    public int getMaxSanity() {
        return maxSanity;
    }

    public void setMaxSanity(int maxSanity) {
        this.maxSanity = maxSanity;
    }

    public int getLuck() {
        return luck;
    }

    public void setLuck(int luck) {
        this.luck = luck;
    }

    public int getMagicPoints() {
        return magicPoints;
    }

    public void setMagicPoints(int magicPoints) {
        this.magicPoints = magicPoints;
    }

    public int getMaxMp() {
        return maxMp;
    }

    public void setMaxMp(int maxMp) {
        this.maxMp = maxMp;
    }
}

package Character;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import static java.lang.System.currentTimeMillis;

/**
 * A class representing random name generator for an investigators name
 * @author Jeremy Dostal-Sharp
 * @version (23/03/2020)
 */
public class NameGenerator {
    private String firstName = "Fiona";
    private String lastName = "Willbottom";
    private String middleName = "Pierce";
    private boolean mName = true;
    private String FEMALE_FIRST_NAME_LOCATION = "./data/femaleFirstNames.txt";
    private String MALE_FIRST_NAME_LOCATION = "./data/maleFirstNames.txt";
    private String LAST_NAME_LOCATION = "./data/lastNames.txt";
    private Random random;
    private List<String> firstNames;
    private List<String> lastNames;
    private boolean female;


    /**
     * This is the constructor for a random name for either a male or female
     * character.
     * @param female set to true if the character is female.
     * @param mName set to true if you want the character to have a middle name.
     */
    public NameGenerator(boolean female, boolean mName) {
        firstNames = new ArrayList<String>();
        lastNames = new ArrayList<String>();
        setFemale(female);
        setFirstNamesList(female);
        setLastNameList(lastNames);
        this.mName = mName;
        this.random = new Random();
        this.random.setSeed(currentTimeMillis());
        this.firstName = randomFirstName(firstNames, random.nextInt());
        this.lastName = randomLastName(lastNames, random.nextInt());
        if (this.mName) {
            this.middleName = randomMiddleName(firstNames, random.nextInt());
        } else {
            this.middleName = null;
        }
    }

    /**
     * This is the constructor if names are chosen by the player.
     * This is most likely not going to be used
     * @param firstName String for first name.
     * @param middleName String for middle name.
     * @param lastName String for last name.
     */
    public NameGenerator(String firstName, String middleName, String lastName) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }

    /**
     * Generates a new random name for the character.
     */
    public void generateNewName() {
        this.firstName = randomFirstName(getFirstNames(), getRandom().nextInt());
        this.lastName = randomLastName(getLastNames(), getRandom().nextInt());
        if (mName) {
            this.middleName = randomMiddleName(getFirstNames(), getRandom().nextInt());
        }
    }

    /**
     * Changes the list of first names to be the sex set as the boolean
     * @param female a boolean for the names list.
     */
    public void changeFemale(boolean female) {
        if (isFemale() != female) {
            setFemale(female);
            setFirstNamesList(isFemale());
        }
    }

    /**
     * @return whether the character is set to female
     */
    public boolean isFemale() {
        return this.female;
    }

    /**
     * Changes the character to female or male
     * @param female whether the characters is female or not
     */
    public void setFemale(boolean female) {
        this.female = female;
    }

    /**
     * @return the list of first names
     */
    public List<String> getFirstNames() {
        return this.firstNames;
    }

    /**
     * @return the list of last names
     */
    public List<String> getLastNames() {
        return this.lastNames;
    }

    /**
     * Makes a list of all the firstnames in data/*FirstNames.txt
     *
     * @param female gets names from female list if true else male list
     */
    private void setFirstNamesList(boolean female) {
        File firstNameFile;
        if (female) {
            firstNameFile = new File(FEMALE_FIRST_NAME_LOCATION);
        } else {
            firstNameFile = new File(MALE_FIRST_NAME_LOCATION);
        }
        try {
            for (Scanner scan = new Scanner(firstNameFile); scan.hasNext(); ){
                this.firstNames.add(scan.nextLine());
            }
        } catch (FileNotFoundException error) {
            error.printStackTrace();
        }
    }

    /**
     * Makes a list of all the lastnames in data/lastNames.txt
     */
    private void setLastNameList(List<String> lastNames) {
        try {
            for (Scanner scan = new Scanner(new File(LAST_NAME_LOCATION)); scan.hasNext(); ){
                this.lastNames.add(scan.nextLine());
            }
        } catch (FileNotFoundException error) {
            error.printStackTrace();
        }
    }

    /**
     * Gets a random name from the list of first names
     * @param random a random int to choose name with.
     * @return first name of the character.
     */
    private String randomFirstName(List<String> firstNames, int random) {
        int size = firstNames.size();
        return firstNames.get(Math.abs(random) % (size));
    }

    /**
     * Gets a random name from the list of last names
     * @param random a random int to choose name with.
     * @return last name of the character.
     */
    private String randomLastName(List<String> lastNames, int random) {
        int size = lastNames.size();
        return lastNames.get(Math.abs(random) % (size));
    }

    /**
     * Gets a random name from the list of middle names
     * @param random a random int to choose name with
     * @return middle name of the character.
     */
    private String randomMiddleName(List<String> firstNames, int random) {
        int size = firstNames.size();
        return firstNames.get(Math.abs(random) % size);
    }

    /**
     * Sets the fist name of this object
     * @param firstName a string for the first name of the character
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Sets the last name of this object
     * @param lastName a string for the last name of the character.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Sets the middle name of this object
     * @param middleName a string for the middle name of the character.
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * Gets the first name of this object
     * @return String that is the first name.
     */
    public String getFirstName() {
        return this.firstName;
    }

    /**
     * Gets the last name of this object
     * @return String that is the last name.
     */
    public String getLastName() {
        return this.lastName;
    }

    /**
     * Gets the middle name of the object
     * @return String that is the middle name or null if no middle name.
     */
    public String getMiddleName() {
        if (this.mName) {
            return this.middleName;
        } else {
            return null;
        }
    }

    /**
     * @return a Random object type
     */
    public Random getRandom() {
        return random;
    }

    public boolean ismName() {
        return mName;
    }

    public void setmName(boolean mName) {
        this.mName = mName;
    }

    @Override
    public String toString() {
        if (this.mName) {
            return getFirstName() + " " + getLastName();
        }
        return getFirstName() + " "
                + getMiddleName() + " "
                + getLastName();
    }
}